// export * from './images'
// export * from './icons'
import Logo from './Logo.png'
import SplashBackground from './SplashBackground.png'
import HeaderLogo from './HeaderLogo.png'
import HeaderBackground from './HeaderBackground.png'
import UserImage from './UserImage.png'
import LoginLogo from './LoginLogo.png'
import LogoAbout from './LogoAbout.png'

export { 
    Logo, SplashBackground, HeaderLogo, 
    HeaderBackground, UserImage, LoginLogo, 
    LogoAbout,
}