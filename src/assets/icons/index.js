import IconHome from './Home.svg';
import IconHomeActive from './HomeActive.svg';
import IconSetting from './Setting.svg'
import IconSettingActive from './SettingActive.svg'
import IconProfile from './Profile.svg'
import IconProfileActive from './ProfileActive.svg'
import IconEdit from './Edit.svg'
import IconLogout from './Logout.svg'
import IconInfo from './Danger-Circle'
import IconArrowRight from './ArrowRight'
import IconBack from './Back'
import IconAbsen from './Absen'
import IconHistory from './History'
import IconWork from './Work'

export { 
    IconHome, IconHomeActive, IconSetting, IconSettingActive, IconProfile, IconProfileActive, IconEdit,
    IconLogout, IconInfo, IconArrowRight, IconBack, IconAbsen, IconWork, IconHistory,
}