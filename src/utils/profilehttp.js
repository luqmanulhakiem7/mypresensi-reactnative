import axios from "axios";
import {AuthContext} from '../../context/AuthContext';

const {userInfo} = useContext(AuthContext);


export default axios.create({
  baseURL: "http://localhost:8080/api",
  headers: {
    "Content-type": "application/json"
    "Authorization": `Bearer ${userInfo.access_token}`
  }
});