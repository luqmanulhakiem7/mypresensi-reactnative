import http from "./profilehttp";

const create = data => {
  return http.post("/profile", data);
};

const profile = {
  create,
};

export default profile;