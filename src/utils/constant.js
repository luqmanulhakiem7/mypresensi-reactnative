export const WARNA_UTAMA = '#fff';
export const WARNA_DISABLE = '#1D1D42';
export const WARNA_TEXT = 'black';
export const WARNA_NAV = '#4E2ECF';
// export const WARNA_BG = '#1D1D42';
export const WARNA_BG = '#f5f5f5';
export const WARNA_CARD = '#fff';

