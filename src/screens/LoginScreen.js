import React, {useContext, useState} from 'react';
import {
  Button,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
  Image,
} from 'react-native';
import { LoginLogo } from '../assets';
import { WARNA_DISABLE, WARNA_NAV, WARNA_TEXT, WARNA_UTAMA } from '../utils/constant';
import Spinner from 'react-native-loading-spinner-overlay';
import {AuthContext} from '../context/AuthContext';

const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const {userInfo, isLoading, login} = useContext(AuthContext);


  return (
    <View style={styles.container}>
      <Spinner visible={isLoading}/>
      <Image source={LoginLogo} style={styles.logo}/>
      <Text style={styles.errmsg}>{userInfo.message}</Text>
      <TextInput
        style={styles.input}
        keyboardType="email-address"
        value={email}
        placeholder="Enter email"
        onChangeText={text => setEmail(text)}
      />
      <TextInput
        style={styles.input}
        keyboardType="password"
        value={password}
        placeholder="Enter password"
        onChangeText={text => setPassword(text)}
        secureTextEntry
      />
      <TouchableOpacity style={styles.forgot} onPress={() => navigation.navigate("Forgot")}>
          <Text style={styles.forgotTxt}>Lupa Password ?</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btn} onPress={() => {login(email, password);}} >
        <Text style={styles.btntext}>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: WARNA_DISABLE,
  },
  logo: {
    marginBottom: 20,
  },
  input: {
    margin: 10,
    padding: 8,
    backgroundColor: WARNA_UTAMA,
    borderRadius: 5,
    width: '60%',
    paddingLeft: 15,
    marginBottom: 30,
  },
  forgot: {
    marginTop: -15,
  },
  errmsg: {
    color: WARNA_UTAMA,
    width: 250,
    textAlign: 'center',

  },
  forgotTxt: {
    paddingRight:120,
    color: WARNA_UTAMA,
    textDecorationLine: "underline",
  },
  btn: {
    marginTop: 50,
    padding: 10,
    backgroundColor: WARNA_UTAMA,
    width: 100,
    borderRadius: 8,
  },
  btntext: {
    textAlign: 'center',
    textTransform: 'uppercase',
    color: WARNA_TEXT,
  }
});

export default LoginScreen;
