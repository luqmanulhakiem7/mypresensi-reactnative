import React from 'react';
import {ActivityIndicator, View} from 'react-native';

const SplashScreen = () => {
  return (
    <View
      style={{flex: 1, justifyContent: 'center', backgroundColor: '#f5f5f5'}}>
      <ActivityIndicator size="large" color="#f5f5f5" />
    </View>
  );
};

export default SplashScreen;
