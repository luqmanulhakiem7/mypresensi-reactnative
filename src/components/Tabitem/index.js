import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { IconHome, IconHomeActive, IconSetting, IconSettingActive, IconProfile, IconProfileActive } from '../../assets/icons'
import { WARNA_UTAMA, WARNA_DISABLE } from '../../utils/constant'

const Tabitem = ({isFocused, onPress, onLongPress, label}) => {
    const Icon = () => {
      if(label === "Settings") return isFocused ? <IconSettingActive style={styles.color}/> : <IconSetting />
      if(label === "Home") return isFocused ? <IconHomeActive style={styles.color}/> : <IconHome />
      if(label === "My Profile") return isFocused ? <IconProfileActive style={styles.color}/> : <IconProfile />

        return <IconHome />
    };

  return (
    <TouchableOpacity
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.container}
          >
            <Icon />
            <Text style={styles.txt(isFocused)}>
              {label}
            </Text>
          </TouchableOpacity>
  )
}

export default Tabitem

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  txt: (isFocused) => ({
    fontSize:  9,
    color: isFocused ? WARNA_UTAMA : WARNA_DISABLE,
    marginTop: 6, 
  }),
  color: {
  }
})