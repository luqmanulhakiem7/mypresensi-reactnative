import { StyleSheet, Text, View } from 'react-native'
import React, { useContext } from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Home, Splash, Login, Settings, MyProfile, EditProfile, About, Masuk, Forgot, Countdown, Panduan, History } from '../pages';
import BottomNavigator from '../components/BottomNavigator';
import {AuthContext} from '../context/AuthContext';
import SplashScreen from '../screens/SplashScreen';
import RegisterScreen from '../screens/RegisterScreen';
import LoginScreen from '../screens/LoginScreen';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();


const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />} initialRouteName='Home'>
        <Tab.Screen name="My Profile" component={MyProfile} options={{ headerShown: false }}/>
        <Tab.Screen name="Home" component={Home} options={{ headerShown: false }}/>
        <Tab.Screen name="Settings" component={Settings} options={{ headerShown: false }}/>
      </Tab.Navigator>
  )
};

const Router = () => {

  const {userInfo, splashLoading} = useContext(AuthContext);


  return (
    <Stack.Navigator>
        {splashLoading ? (
          <Stack.Screen name="SplashScreen" component={SplashScreen} options={{ headerShown: false }}/>
          ) : userInfo.access_token ? (
            <>
            <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }}/>
            <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }}/>
            <Stack.Screen name="EditProfile" component={EditProfile} options={{ headerShown: false }}/>
            <Stack.Screen name="Masuk" component={Masuk} options={{ headerShown: false }}/>
            <Stack.Screen name="About" component={About} options={{ headerShown: false }}/>
            <Stack.Screen name="Panduan" component={Panduan} options={{ headerShown: false }}/>
            <Stack.Screen name="History" component={History} options={{ headerShown: false }}/>
          </>
        ) : (
          <>
            <Stack.Screen name="LoginScreen" component={LoginScreen} options={{ headerShown: false }}/>
            <Stack.Screen name="Forgot" component={Forgot} options={{ headerShown: false }}/>
            <Stack.Screen name="Countdown" component={Countdown} options={{ headerShown: false }}/>

            <Stack.Screen
              name="Register"
              component={RegisterScreen}
              options={{headerShown: false}}
            />
          </>
        )}
      </Stack.Navigator>
  )
};

export default Router

const styles = StyleSheet.create({})