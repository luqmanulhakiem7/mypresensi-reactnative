import { StyleSheet, Text, View, TouchableOpacity, Image, ScrollView } from 'react-native'
import React from 'react'
import { WARNA_BG, WARNA_CARD, WARNA_NAV, WARNA_TEXT, WARNA_UTAMA } from '../../utils/constant'
import { IconBack, LogoAbout } from '../../assets'


const About = ({navigation}) => {
  return (
    <View style={styles.container}>
        <View style={styles.title}>
            <TouchableOpacity
            style={styles.btnBack}
            onPress={() => navigation.goBack()}
            >
                <IconBack />
            </TouchableOpacity>
            <Text style={styles.titleTxt}>Panduan Penggunaan</Text>
        </View>
        <ScrollView style={styles.card}>
            <Image source={LogoAbout}/>
            <View style={styles.paragraph}>
                <Text style={styles.p}>
                    Cara Melakakukan Absen:
                </Text>
                <Text style={styles.p}>
                    1. Pastikan anda menyalakan gps pada device anda.
                </Text>
                <Text style={styles.p}>
                    2. Pastikan aplikasi diberi izin untuk mengakses lokasi anda.
                </Text>
                <Text style={styles.p}>
                    3. Pencet tombol "Absen Masuk" untuk melakukan absen masuk.
                </Text>
                <Text style={styles.p}>
                    4. Pencet tombol "Absen Pulang" untuk melakukan absen pulang.
                </Text>
            </View>
            <View>
                <Text></Text>
            </View>
        </ScrollView>
    </View>
  )
}

export default About

const styles = StyleSheet.create({
    container: {
    backgroundColor: WARNA_BG,
    height: '100%',
    flex: 1,
  },

  // Header
  title: {
    backgroundColor: WARNA_NAV,
    height: 50,
    padding: 10,
    flexDirection: 'row',
  },
  btnBack: {
    justifyContent: 'center',
    paddingRight: 12,
  },
  titleTxt: {
    color: WARNA_UTAMA,
    fontSize: 20,
    fontWeight: 'bold',
  },

    // Card
    card: {
        margin: 10,
        padding: 10,
        marginTop: 30,
    },
    paragraph: {
        marginTop: 15,
    },
    h1: {
        fontSize: 30,
        fontWeight: 'bold',
        color: WARNA_TEXT,
        marginBottom: 5,
        fontFamily: 'monospace',
    },
    p: {
        fontFamily: 'monospace',
        color: WARNA_TEXT,
        fontSize: 20,
        textAlign: 'justify',
    },

})