import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { WARNA_DISABLE, WARNA_NAV, WARNA_UTAMA } from '../../utils/constant';


const Forgot = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Opsss</Text>
      <Text style={styles.text}>Silahkan hubungi admin untuk perubahan password</Text>
      <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate("LoginScreen")}>
        <Text style={styles.btntext}>HALAMAN LOGIN</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Forgot

const styles = StyleSheet.create({
    container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: WARNA_DISABLE,
  },
  header: {
    fontSize: 50,
    color: WARNA_UTAMA,
  },
  text: {
    fontSize: 14,
    color: WARNA_UTAMA,
  },
  btn: {
    marginTop: 50,
    padding: 10,
    height: 50,
    width: 150,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: WARNA_NAV,
    borderRadius: 8,
  },
  btntext: {
    textAlign: 'center',
    textTransform: 'uppercase',
    color: WARNA_UTAMA,
  }
})