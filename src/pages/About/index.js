import { StyleSheet, Text, View, TouchableOpacity, Image, ScrollView } from 'react-native'
import React from 'react'
import { WARNA_BG, WARNA_CARD, WARNA_NAV, WARNA_TEXT, WARNA_UTAMA } from '../../utils/constant'
import { IconBack, LogoAbout } from '../../assets'


const About = ({navigation}) => {
  return (
    <View style={styles.container}>
        <View style={styles.title}>
            <TouchableOpacity
            style={styles.btnBack}
            onPress={() => navigation.goBack()}
            >
                <IconBack />
            </TouchableOpacity>
            <Text style={styles.titleTxt}>About</Text>
        </View>
        <ScrollView style={styles.card}>
            <Image source={LogoAbout}/>
            <View style={styles.paragraph}>
                <Text style={styles.h1}>Apa Itu Aplikasi MyPresensi ?</Text>
                <Text style={styles.p}>
                    Aplikasi yang mencatat kehadiran pegawai serta menyajikan laporan kehadiran pegawai, yang dimana aplikasi ini berbasis online.
                </Text>
                </View>
            <View style={styles.paragraph}>
                <Text style={styles.h1}>Mengapa harus menggunakan aplikasi MyPresensi ?</Text>
                <Text style={styles.p}>
                    Karena mempermudah pegawai untuk melakukan absen serta mempermudah perusahaan untuk menghemat tenaga untuk membuat laporan rekap.
                </Text>
            </View>
            <View style={styles.paragraph}>
                <Text style={styles.h1}>Siapa Saja Yang Bersangkutan Dalam Pembuatan MyPresensi ?</Text>
                <Text style={styles.p}>
                    Aplikasi MyPresensi dibuat oleh beberapa mahasiswa Universitas Islam Madura. diantaranya yaitu:
                </Text>
                <Text style={styles.p}>
                    - Luqmanul Hakiem Sebagai Project Manager
                </Text>
                <Text style={styles.p}>
                    - Sulfan Wahyudi sebagai UI/UX Designer
                </Text>
                <Text style={styles.p}>
                    - Munadi sebagai Graphic Designer
                </Text>
                <Text style={styles.p}>
                    - Rudi sebagai Mobile FrontEnd
                </Text>
                <Text style={styles.p}>
                    - Afiful Khoir sebagai Mobile Backend
                </Text>
                <Text style={styles.p}>
                    - Lutfi sebagai Mobile Backend
                </Text>
            </View>
            <View>
                <Text></Text>
            </View>
        </ScrollView>
    </View>
  )
}

export default About

const styles = StyleSheet.create({
    container: {
    backgroundColor: WARNA_BG,
    height: '100%',
    flex: 1,
  },

  // Header
  title: {
    backgroundColor: WARNA_NAV,
    height: 50,
    padding: 10,
    flexDirection: 'row',
  },
  btnBack: {
    justifyContent: 'center',
    paddingRight: 12,
  },
  titleTxt: {
    color: WARNA_UTAMA,
    fontSize: 20,
    fontWeight: 'bold',
  },

    // Card
    card: {
        margin: 10,
        padding: 10,
        marginTop: 30,
    },
    paragraph: {
        marginTop: 15,
    },
    h1: {
        fontSize: 30,
        fontWeight: 'bold',
        color: WARNA_TEXT,
        marginBottom: 5,
        fontFamily: 'monospace',
    },
    p: {
        fontFamily: 'monospace',
        color: WARNA_TEXT,
        fontSize: 20,
        textAlign: 'justify',
    },

})