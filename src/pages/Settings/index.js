import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useContext } from 'react'
import { WARNA_BG, WARNA_CARD, WARNA_NAV, WARNA_TEXT, WARNA_UTAMA } from '../../utils/constant'
import { IconArrowRight, IconHistory, IconInfo, IconLogout } from '../../assets'
import {AuthContext} from '../../context/AuthContext';


const Settings = ({navigation}) => {
  const {logout} = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.titleTxt}>Settings</Text>
      </View>
      <View style={styles.cardUtama}>
        <TouchableOpacity style={styles.btn}
        onPress={() =>
        navigation.navigate('History')}
        >
          <IconHistory />
          <Text style={{ color: WARNA_TEXT, paddingLeft: 15, paddingRight: 180,}}>History</Text>
          <IconArrowRight />
        </TouchableOpacity>
        <TouchableOpacity style={styles.btn}
        onPress={() =>
        navigation.navigate('Panduan')}
        >
          <IconInfo />
          <Text style={{ color: WARNA_TEXT, paddingLeft: 15, paddingRight: 180,}}>Panduan Penggunaan</Text>
          <IconArrowRight />
        </TouchableOpacity>
      </View>
      <View style={styles.cardUtama}>
        <TouchableOpacity style={styles.btn} onPress={logout}>
          <IconLogout />
          <Text style={{ color: WARNA_TEXT, paddingLeft: 15, paddingRight: 180, }}>Keluar</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footertxt}>v1.0.0</Text>
      </View>
    </View>
  )
}

export default Settings

const styles = StyleSheet.create({
  container: {
    backgroundColor: WARNA_BG,
    height: '100%',
    flex: 1,
  },

  // Header
  title: {
    backgroundColor: WARNA_NAV,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleTxt: {
    color: WARNA_UTAMA,
    fontSize: 20,
    fontWeight: 'bold',
  },

  // Profile Card
  cardUtama: {
    marginTop: 30,
  },
  btn: {
    marginTop: 5,
    flexDirection: 'row',
    padding: 20,
        shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    backgroundColor: WARNA_CARD,
    alignItems: 'center',
  },

  // Footer
  footer: {
    position: 'absolute',
    bottom: 20,
    alignSelf: 'center',
  },
  footertxt: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.05)',
  },
})