import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert, Image } from 'react-native'
import React, {Component} from 'react'
import api from '../../utils/api';
import { WARNA_DISABLE, WARNA_NAV, WARNA_UTAMA } from '../../utils/constant';
import { LoginLogo } from '../../assets';

export class Login extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       email: '',
       password: '',
    }
  }

  getData = async () => {
    const {email, password} = this.state;
    const data = {email, password}
    // Alert.alert('info', 'email ' + email);
    try {
      let res = await api.post('api/login', data);
      console.log(res);
      if (res) {
        this.props.navigation.navigate('Splash');
      }
    } catch (error) {
      console.log('network eror')
    }
  }

  render() {
    return (
    <View style={styles.body}>
      <Image source={LoginLogo} style={styles.logo}/>
      <TextInput
        style={styles.input}
        placeholder="Email"
        keyboardType="email-address"
        onChangeText={(text) => this.setState({email: text})}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        keyboardType="password"
        onChangeText={(text) => this.setState({password: text})}
      />
      <TouchableOpacity style={styles.btn} onPress={() => this.getData()}>
        <Text style={styles.btntext}>Login</Text>
      </TouchableOpacity>
    </View>
    )
  }
}

export default Login

const styles = StyleSheet.create({
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: WARNA_DISABLE,
  },
  logo: {
    marginBottom: 20,
  },
  input: {
    margin: 10,
    padding: 8,
    backgroundColor: WARNA_NAV,
    borderRadius: 5,
    width: '60%',
    paddingLeft: 15,
    marginBottom: 30,
  },
  btn: {
    marginTop: 50,
    padding: 10,
    backgroundColor: WARNA_NAV,
    width: 100,
    borderRadius: 8,
  },
  btntext: {
    textAlign: 'center',
    textTransform: 'uppercase',
    color: WARNA_UTAMA,
  }
})
