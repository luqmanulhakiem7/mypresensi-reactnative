import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'
import { SplashBackground, Logo } from '../../assets'
const Splash = ({navigation}) => {

  useEffect(() => {
      setTimeout( () => {
        navigation.replace('MainApp');
      }, 2000);
  }, [navigation]);

  return (
    <ImageBackground source={SplashBackground} style={styles.bgImage}>
      <Image source={Logo} style={styles.lgo}/>
    </ImageBackground>
  )
}

export default Splash

const styles = StyleSheet.create({
  bgImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lgo: {
    width: 100,
    height: 100,
  }

})