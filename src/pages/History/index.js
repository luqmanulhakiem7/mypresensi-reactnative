import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Image } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import { WARNA_BG, WARNA_CARD, WARNA_NAV, WARNA_TEXT, WARNA_UTAMA } from '../../utils/constant'
import { IconBack, LogoAbout } from '../../assets'
import {AuthContext} from '../../context/AuthContext';
import { IconAbsen, IconWork } from '../../assets'
import {BASE_URL} from '../../config';
import axios from 'axios';


const History = ({navigation}) => {
  const {userInfo} = useContext(AuthContext);
    const [history, setHistory] = useState([]);
    const formatResponse = (res) => {
        return JSON.stringify(res, null, 2);
    };

    useEffect(() => {
        getData()
    }, [])

  async function getData() {
    try {
      const res = await axios.get(`${BASE_URL}/history`, {
        headers : {
          "Content-Type" : "application/json",
          "Authorization" : `Bearer ${userInfo.access_token}`
        }
      });
      const result = {
        data: res.data,
      };
       if (res) {
        setHistory(res.data.data)
      }

    } catch(err) {
        console.log(err)
      setHistory(formatResponse(err.response?.data || err));
    }
  }
  return (
    <View style={styles.container}>
        <View style={styles.title}>
            <TouchableOpacity
            style={styles.btnBack}
            onPress={() => navigation.goBack()}
            >
                <IconBack />
            </TouchableOpacity>
            <Text style={styles.titleTxt}>History</Text>
        </View>
        <ScrollView>
            {history.map((item, key) => {
                return (

            <View style={styles.card} key={key}>
              <View>
                {item.status === 'Hadir' ? (
                  <View style={styles.iconAbsen}>
                    <IconAbsen />
                  </View>
                ): (
                  <View style={styles.iconWork}>
                    <IconWork />
                  </View>
                )}
              </View>
              <View style={styles.infoAbsen}>
                  <Text style={{color: WARNA_TEXT, fontWeight: 'bold',}}>{userInfo.name}</Text>
                  <Text style={{color: WARNA_TEXT,}}>{item.tgl} || {item.status}</Text>
              </View>
            </View>
                )
            })}
            <TouchableOpacity>
            </TouchableOpacity>
        </ScrollView>
        
    </View>
  )
}

export default History


const styles = StyleSheet.create({
    container: {
    backgroundColor: WARNA_BG,
    height: '100%',
    flex: 1,
  },

  // Header
  title: {
    backgroundColor: WARNA_NAV,
    height: 50,
    padding: 10,
    flexDirection: 'row',
  },
  btnBack: {
    justifyContent: 'center',
    paddingRight: 12,
  },
  titleTxt: {
    color: WARNA_UTAMA,
    fontSize: 20,
    fontWeight: 'bold',
  },

  card: {
    margin: 30,
    padding: 10,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,

    backgroundColor: WARNA_BG,
    flexDirection: 'row',
  },
  iconWork: {
    borderColor: 'black',
    borderWidth: 2,
    borderRadius: 10,
    backgroundColor: 'yellow',
    padding: 10,
  },
  iconAbsen: {
    borderColor: 'black',
    borderWidth: 2,
    borderRadius: 10,
    backgroundColor: 'green',
    padding: 10,
  },

  infoAbsen: {
    marginLeft: 10,
    padding: 10,
  },

})