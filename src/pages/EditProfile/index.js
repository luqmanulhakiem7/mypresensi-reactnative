import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View, Image } from 'react-native';
import { WARNA_BG, WARNA_NAV, WARNA_TEXT, WARNA_UTAMA } from '../../utils/constant';
import { IconBack } from '../../assets';
import { UserImage } from '../../assets';
import {AuthContext} from '../../context/AuthContext';
import {BASE_URL} from '../../config';
import axios, { Axios } from 'axios';

const EditProfile = ({navigation}) => {
  const [name, setName] = useState("");
  const [nik, setNik] = useState("");
  const [fotouser, setFotouser] = useState("");
  const [alamat, setAlamat] = useState("");
  const [hp, setHp] = useState("");
  const [ttl, setTtl] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const {userInfo, eProfile, isLoading} = useContext(AuthContext);
  const [datauser, setDatauser] = useState([]);
  const formatResponse = (res) => {
    return JSON.stringify(res, null, 2);
  };

  useEffect(() => {
    getData()
  }, [])

  async function getData() {
    try {
      const res = await axios.get(`${BASE_URL}/dprofil`, {
        headers : {
          "Content-Type" : "application/json",
          "Authorization" : `Bearer ${userInfo.access_token}`
        }
      });
      const result = {
        data: res.data,
      };
      console.log(res);
      if (res) {
        setName(res.data.data.name)
        setNik(res.data.data.nik)
        setFotouser(res.data.data.fotouser)
        setAlamat(res.data.data.alamat)
        setHp(res.data.data.hp)
        setTtl(res.data.data.ttl)
        setEmail(res.data.data.email)
      }

      setDatauser(formatResponse(result));
    } catch(err) {
      setDatauser(formatResponse(err.response?.data || err));
    }
  }

  return (
    <View style={styles.container}>
        <View style={styles.title}>
            <TouchableOpacity
            style={styles.btnBack} onPress={() => navigation.goBack()}>
                <IconBack />
            </TouchableOpacity>
            <Text style={styles.titleTxt}>Edit Profile</Text>
            <TouchableOpacity style={styles.btnSimpan}>
                <Text style={{color: WARNA_TEXT,}} onPress={() => {eProfile(name, nik, alamat, hp, ttl, email, password)}}>Simpan</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.content}>
            <View style={styles.card}>
                <Text style={styles.Text}>Foto Profil</Text>
                <View style={{ flexDirection: 'row', }}>
                    {fotouser === null ? (
                      <Image source={UserImage}  style={styles.UserImg}/>
                    ): (
                      <Image source={{uri:"http://young-plains-79433.herokuapp.com/foto_user/" + fotouser,} } style={styles.UserImg}/>
                    )}
                    <Text style={styles.PhotoDesc}>Ganti Foto Anda Dari Website</Text>
                </View>
                {/* <TouchableOpacity style={{ paddingLeft: 8, }}>
                    <Text>Ganti</Text>
                </TouchableOpacity> */}
            </View>
            
            <View style={styles.card}>
                 
                <Text style={styles.Text}>
                    Nama Lengkap<Text style={styles.red}>*</Text>
                </Text>
                <TextInput 
                    style={styles.input}
                    placeholder="Masukkan Nama Lengkap Kamu"
                    keyboardType="text"
                    value={name}
                    onChangeText={(value) => setName(value)}
                />
            </View>
            <View style={styles.card}>
                <Text style={styles.Text}>
                    NIK<Text style={styles.red}>*</Text>
                </Text>
                <TextInput 
                    style={styles.input}
                    placeholder="Masukkan NIK Anda"
                    keyboardType="text"
                    value={nik}
                    onChangeText={(value) => setNik(value)}
                />
            </View>
            <View style={styles.card}>
                <Text style={styles.Text}>
                    Alamat<Text style={styles.red}>*</Text>
                </Text>
                <TextInput 
                    style={styles.input}
                    placeholder="Cth: Jln. Aspal Hitam"
                    keyboardType="text"
                    value={alamat}
                    onChangeText={(value) => setAlamat(value)}
                />
            </View>
            <View style={styles.card}>
                <Text style={styles.Text}>
                    Tempat, Tanggal Lahir<Text style={styles.red}>*</Text>
                </Text>
                <TextInput 
                    style={styles.input}
                    placeholder="Cth: Bekasi, 17 Oktober 1969"
                    keyboardType="text"
                    value={ttl}
                    onChangeText={(value) => setTtl(value)}
                />
            </View>
            <View style={styles.card}>
                <Text style={styles.Text}>
                    No. Hp<Text style={styles.red}>*</Text>
                </Text>
                <TextInput 
                    style={styles.input}
                    placeholder="Cth: 08XXYYYYXXXX"
                    keyboardType="number-pad"
                    value={hp}
                    onChangeText={(value) => setHp(value)}
                />
            </View>
            <View style={styles.card}>
                <Text style={styles.Text}>
                    Email<Text style={styles.red}>*</Text>
                </Text>
                <TextInput 
                    style={styles.input}
                    placeholder="Masukkan Email Anda"
                    keyboardType="email-address"
                    value={email}
                    onChangeText={(value) => setEmail(value)}
                />
            </View>
            <View style={styles.card}>
                <Text style={styles.Text}>
                    Password<Text style={styles.red}>*</Text>
                </Text>
                <TextInput
                  style={styles.input}
                  placeholder="Masukkan Password Anda"
                  keyboardType="password"
                  value={password}
                  onChangeText={(value) => setPassword(value)}
                  secureTextEntry
                />
            </View>
        </View>
    </View>
  )
}

export default EditProfile

const styles = StyleSheet.create({
    container: {
    backgroundColor: WARNA_BG,
    height: '100%',
    flex: 1,
  },

  // Header
  title: {
    backgroundColor: WARNA_NAV,
    height: 50,
    padding: 10,
    flexDirection: 'row',
  },
  btnBack: {
    justifyContent: 'center',
    paddingRight: 12,
  },
  titleTxt: {
    color: WARNA_UTAMA,
    fontSize: 20,
    fontWeight: 'bold',
  },
  btnSimpan: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: WARNA_BG,
    padding: 5,
    width: 80,
    marginLeft: 150,
  },

  content: {
    padding: 10,
  },
  card: {
    marginTop: 15,
  },
  red: {
    color: 'red',
  },
  Text: {
    color: WARNA_TEXT,
    marginBottom: 5,
    fontWeight: 'bold',
  },
  PhotoDesc: {
    marginLeft: 10,
  },

UserImg: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },
input: {
    padding: 8,
    borderBottomWidth: 1,
    borderBottomColor: WARNA_NAV,
    width: '100%',
  },
})
