import { StyleSheet, Image, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React, { useContext, useEffect, useState } from 'react'
import { WARNA_BG, WARNA_CARD, WARNA_NAV, WARNA_TEXT, WARNA_UTAMA } from '../../utils/constant'
import { UserImage, IconEdit } from '../../assets'
import {BASE_URL} from '../../config';
import {AuthContext} from '../../context/AuthContext';
import axios from 'axios';


const MyProfile = ({navigation}) => {
  const [name, setName] = useState("");
  const [fotouser, setFotouser] = useState("");
  const [email, setEmail] = useState("");
  const {userInfo, eProfile, isLoading} = useContext(AuthContext);
  const [datauser, setDatauser] = useState([]);
  const formatResponse = (res) => {
    return JSON.stringify(res, null, 2);
  };

  useEffect(() => {
    getData()
  }, [])

  async function getData() {
    try {
      const res = await axios.get(`${BASE_URL}/dprofil`, {
        headers : {
          "Content-Type" : "application/json",
          "Authorization" : `Bearer ${userInfo.access_token}`
        }
      });
      const result = {
        data: res.data,
      };
      console.log(res);
      if (res) {
        setName(res.data.data.name)
        setFotouser(res.data.data.fotouser)
        setEmail(res.data.data.email)
      }

      setDatauser(formatResponse(result));
    } catch(err) {
      setDatauser(formatResponse(err.response?.data || err));
    }
  }
  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Text style={styles.titleTxt}>MyProfile</Text>
      </View>
      <View style={styles.card}>
        <View style={styles.UserImage}>
          {fotouser === null ? (
            <Image source={UserImage}  style={styles.UserImg}/>
          ): (
            <Image source={{uri:"http://young-plains-79433.herokuapp.com/foto_user/" + fotouser,} } style={styles.UserImg}/>
          )}
        </View>
        <View style={styles.UserName}>
          <Text style={styles.uName}>{name}</Text>
          <Text style={styles.uMail}>{email}</Text>
        </View>
        <View style={styles.UserAct}>
          <TouchableOpacity
          onPress={() => navigation.navigate('EditProfile')}
          >
            <IconEdit />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

export default MyProfile

const styles = StyleSheet.create({
  container: {
    backgroundColor: WARNA_BG,
    height: '100%',
    flex: 1,
  },

  // Header
  title: {
    backgroundColor: WARNA_NAV,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleTxt: {
    color: WARNA_UTAMA,
    fontSize: 20,
    fontWeight: 'bold',
  },

  // Profile Card
  card: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    margin: 10,
    marginTop: 30,
    borderRadius: 20,
    flexDirection: 'row',
    padding: 20,
    backgroundColor: WARNA_BG,

  },
  UserImage: {
    justifyContent: 'center',
  },
  UserImg: {
    width: 50,
    height: 50,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 50,
  },
  UserName: {
    paddingLeft: 10,
    justifyContent: 'center',
    width: 230,
    overflow: 'hidden',
  },
  uName: {
    fontWeight: 'bold',
    color: WARNA_TEXT,
    
  },
  uMail: {
    fontSize: 12,
    color: 'grey',
  },

  UserAct: {
    paddingLeft: 20,
    position: 'relative',
    justifyContent: 'center',
  },
})