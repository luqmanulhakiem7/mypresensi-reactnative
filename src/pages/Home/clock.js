import { Text, StyleSheet, View } from 'react-native'
import React, { Component } from 'react'

export default class Clock extends React.Component {
      constructor(props) {
        super(props);
        this.state = {
          day: new Date().getDate(),
          month: new Date().getMonth() + 1,
          year: new Date().getFullYear(),
          hour: new Date().getHours(),
          min: new Date().getMinutes(),
          sec: new Date().getSeconds(),
        };
      }
      componentDidMount() {
        this.intervalID = setInterval(
          () => this.tick(),
          1000
        );
      }
      componentWillUnmount() {
        clearInterval(this.intervalID);
      }
      tick() {
        this.setState({
            day: new Date().getDate(),
            month: new Date().getMonth() + 1,
            year: new Date().getFullYear(),
            hour : new Date().getHours(),
            min : new Date().getMinutes(),
            sec : new Date().getSeconds(),
        });
      }
      render() {
        return (
            <View style={styles.card}>
                <Text style={styles.txt}>

                   Tanggal : {this.state.day}/{this.state.month}/{this.state.year}
                </Text>
                <Text style={styles.txt}>
                   Jam     : {this.state.hour}:{this.state.min}:{this.state.sec}
                </Text>
            </View>
        );
      }
    }

const styles = StyleSheet.create({
    card: {
        backgroundColor: 'rgb(78, 46, 207)',
        borderRadius: 6,
        margin: 10,
        padding: 10,
    },
    txt: {
        textTransform: 'uppercase',
        fontFamily: 'monospace',
        color: '#FFF',
    }
})