import { StyleSheet, Text, View, TouchableOpacity, Image, Dimensions, PermissionsAndroid, Alert, TextInput } from 'react-native'
import React, { useContext, useState, useEffect } from 'react'
import Clock from './clock'
import { HeaderLogo } from '../../assets'
import { WARNA_BG, WARNA_CARD, WARNA_NAV, WARNA_TEXT } from '../../utils/constant'
import Spinner from 'react-native-loading-spinner-overlay';
import {AuthContext} from '../../context/AuthContext';
import Geolocation from '@react-native-community/geolocation';
import {BASE_URL} from '../../config';
import axios from 'axios'


const Home = ({navigation}) => {
  const {userInfo, Masuk, Pulang} = useContext(AuthContext);
  
    const [
    currentLongitude,
    setCurrentLongitude
  ] = useState('...');
  const [
    currentLatitude,
    setCurrentLatitude
  ] = useState('...');
  const [
    locationStatus,
    setLocationStatus
  ] = useState('');
 
  useEffect(() => {
    const requestLocationPermission = async () => {
      if (Platform.OS === 'ios') {
        getOneTimeLocation();
        subscribeLocationLocation();
      } else {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This Masuk needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
            getOneTimeLocation();
            subscribeLocationLocation();
          } else {
            setLocationStatus('Permission Denied');
          }
        } catch (err) {
          console.warn(err);
        }
      }
    };
    requestLocationPermission();
    return () => {
      Geolocation.clearWatch(watchID);
    };
  }, []);
 
  const getOneTimeLocation = () => {
    setLocationStatus('Getting Location ...');
    Geolocation.getCurrentPosition(
      //Will give you the current location
      (position) => {
        setLocationStatus('You are Here');
 
        //getting the Longitude from the location json
        const currentLongitude = 
          JSON.stringify(position.coords.longitude);
 
        //getting the Latitude from the location json
        const currentLatitude = 
          JSON.stringify(position.coords.latitude);
 
        //Setting Longitude state
        setCurrentLongitude(currentLongitude);
        
        //Setting Longitude state
        setCurrentLatitude(currentLatitude);
      },
      (error) => {
        setLocationStatus(error.message);
      },
    );
  };
 
  const subscribeLocationLocation = () => {
    watchID = Geolocation.watchPosition(
      (position) => {
        //Will give you the location on location change
        
        setLocationStatus('You are Here');
        console.log(position);
 
        //getting the Longitude from the location json        
        const currentLongitude =
          JSON.stringify(position.coords.longitude);
 
        //getting the Latitude from the location json
        const currentLatitude = 
          JSON.stringify(position.coords.latitude);
 
        //Setting Longitude state
        setCurrentLongitude(currentLongitude);
 
        //Setting Latitude state
        setCurrentLatitude(currentLatitude);
      },
      (error) => {
        setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        maximumAge: 1000
      },
      );
    };

  const handlemasuk = () => {
    var data = {
      "locm": currentLatitude+","+currentLongitude,
    }
    axios({
      url:`${BASE_URL}/absenmasuk`,
      method:'POST',
      data: data,
      headers : {
        "Content-Type" : "application/json",
        "Authorization" : `Bearer ${userInfo.access_token}`
      }
    }).then((res)=>{
      Alert.alert("Succes", "Absen Masuk Berhasil")
      console.log('succes')
    }).catch(e => {
        Alert.alert("Error", "Absen Gagal")
        console.log(`Eror Absen masuk ${e}`);
      });
  }
  
  const handlepulang = () => {
    var data = {
      "locp": currentLatitude+","+currentLongitude,
    }
    axios({
      url:`${BASE_URL}/absenpulang`,
      method:'POST',
      data: data,
      headers : {
        "Content-Type" : "application/json",
        "Authorization" : `Bearer ${userInfo.access_token}`
      }
    }).then((res)=>{
      Alert.alert("Succes", "Absen Pulang Berhasil")
      console.log('succes')
    }).catch(e => {
        Alert.alert("Error", "Absen Gagal")
        console.log(`Eror Absen masuk ${e}`);
      });
  }
  const [locm, setLocm] = useState("");
  const [locp, setLocp] = useState("");
  
  const onChangeLocm = (value) => {
    setLocm(value)
  }
  
  const onChangeLocp = (value) => {
    setLocm(value)
  }
    return (
    <View style={styles.container}>
     
      <View style={styles.HeaderBg}>
        <Image source={HeaderLogo} style={styles.HeaderLg}/>
        <Text style={styles.txtWelcome}>Selamat Datang,</Text>
        <Text style={styles.txtUser}>{userInfo.name}</Text>
      </View>
      <View style={styles.jam}>
        <Clock />
        {/* <View>
             <Text>
            Long: {currentLongitude}, Lat: {currentLatitude}
          </Text>
        </View> */}
      </View>
      <View style={styles.btnContainer}>
        {/* <TextInput 
        placeholder='locm'
        value={locm}
        defaultValue={"https://maps.google.com/maps?q=loc:"+currentLatitude+","+currentLongitude}
        onChangeText={onChangeLocm}
        /> */}
       
        <TouchableOpacity style={styles.btn}
        // onPress={() => {Masuk(locm)}}
        onPress={handlemasuk}
        >
          <Text style={styles.btntext}>Absen Masuk</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btn}
        // onPress={() => {Pulang(locp)}}
        onPress={handlepulang}
        >
          <Text style={styles.btntext}>Absen Pulang</Text>
        </TouchableOpacity>
      </View>
      {/* <View>
          <Text
            style={{
              marginTop: 16,
            }}>
            Longitude: {currentLongitude}, Latitude: {currentLatitude}
          </Text>
      </View> */}
    </View>
  )
}

export default Home

const windowWidth =  Dimensions.get('window').width;
const windowHeight =  Dimensions.get('window').height;

const styles = StyleSheet.create({
  HeaderBg: {
    backgroundColor: 'rgb(78, 46, 207)',
    width: windowWidth,
    height: windowHeight*0.25,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    padding: 10,
  }, 
  HeaderLg: {
    width: 120,
    height: 50,
    marginBottom: 25,
  },
  txtWelcome: {
    fontSize: 20,
    color: '#FFF',
  },
  txtUser: {
    fontWeight: 'bold',
    fontSize: 25,
    height: 40,
    overflow: 'hidden',
    color: '#FFF',
  },
  container: {
    backgroundColor: WARNA_BG,
    height: '100%',
    flex: 1,
  },
  title: {
    fontSize: 20,
    marginBottom: 20,
  },
  btnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  jam: {
    marginTop: 10,
    marginBottom: 60,
  },
  btn: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    marginTop: 10,
    padding: 20,
    backgroundColor: WARNA_NAV,
    width: 150,
    borderRadius: 8,
  },
   btntext: {
    color: '#fff',
    textAlign: 'center',
    textTransform: 'uppercase',
  
  }
})